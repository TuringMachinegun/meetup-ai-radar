Installation
============
Tested on python 2.7

these are the dependencies:

- langid
- meetup-api (install patched version from https://github.com/TuringMachinegun/meetup-api)
- pandas
- BeautifulSoup

run the following to install them:

```
pip install langid git+https://github.com/TuringMachinegun/meetup-api pandas python-bs4
```

Configuration
=============

You'll have to get your meetup api token from [here](https://secure.meetup.com/meetup_api/key/)
and paste it in the config.json file.

In the same file, you can also customize the list of topics to use in query.
(See [here](https://www.meetup.com/topics/) for available topics)

Running the script
==================

```
python download_city_meetups_data.py Moscow
```

will produce an output.csv file with relevant data from AI meetups in Moscow.

run
```
python dowload_city_meetups_data.py --help
```

to get help on other available flags.


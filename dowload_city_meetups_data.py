"""A simple script to download AI meetup groups data to a csv file.
"""
from collections import OrderedDict
import argparse
import json

import langid
import meetup.api
import pandas as pd
from bs4 import BeautifulSoup
import pdb

   
def clean_html(text):
    """Strip html markup from text.

    :type text: str
    """
    text = BeautifulSoup(text, features='html.parser').get_text(strip=True)
    return text


def get_groups(client, topic_id, query):
    """Get all groups for topic, with query parameters for meetup.api.Client.GetFindGroups.

    :param client: a meetup.api.Client instance
    :param topic_id: a meetup topic
    :type topic_id: str
    :param query: query arguments for meetup.api.Client.GetFindGroups
    :type query: dict
    """
    groups = client.GetFindGroups(topic_id=topic_id, **query)
    return groups


def get_group_properties(group):
    """Get properties for one group.
    
    :param group: a meetup.api.MeetupGroup object
    :rtype: OrderedDict
    """
    descr = clean_html(group.description)
    props = OrderedDict()

    props['id'] = group.id
    props['name'] = group.name
    props['organizer_name'] = group.organizer['name']
    props['organizer_id'] = group.organizer['id']
    props['category_name'] = group.category['name']
    props['category_id'] = group.category['id']
    props['city'] = group.city
    props['created'] = group.created
    props['language'] = langid.classify(descr)[0]
    props['description'] = descr
    props['join_mode'] = group.join_mode
    props['link'] = group.link
    props['members'] = group.members
    props['meta_category_id'] = group.meta_category['id']
    props['meta_category_name'] = group.meta_category['name']
    props['score'] = group.score
    props['state'] = group.state
    props['status'] = group.status
    props['visibility'] = group.visibility
    props['who'] = group.who

    return props


def get_groups_properties(client, topics, query):
    """Get properties for all groups

    :param client: a meetup.api.Client instance
    :param topics: a list of topics of interest
    :param query: query parameters for meetup.api.Client.GetFindGroups 
    :type query: dict

    :rtype: List[OrderedDict]
    """

    all_topics = client.GetTopics().results

    groups_by_id = {}
    topic_ids = [x['id'] for x in all_topics if x['name'] in topics]    
    for topic, topic_id in zip(topics, topic_ids):
        groups = get_groups(client, topic_id, query)
        for group in groups:
            if group.id not in groups_by_id:
                props = get_group_properties(group)
                props.update({k: 0 for k in topics})
                groups_by_id[group.id] = props
            groups_by_id[group.id][topic] = 1            
    return groups_by_id.values()


def make_dataframe(rows):
    """Organize group information in a pandas dataframe for easy exporting.

    :param rows: a list of dictionaries each representing a row in the final dataframe.
    """    
    df = pd.DataFrame(rows)
    df.sort_values('members', ascending=False, inplace=True)
    df.set_index('id', inplace=True)
    return df


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get information about AI groups in a given city.')
    parser.add_argument(
        'city',
        type=str,
        help='The city of interest.'
    )
    parser.add_argument(
        '--output',
        type=str,
        help='Filename for csv output.',
        default='output.csv',
        
    )
    parser.add_argument(
        '--country',
        type=str,
        help='A country code, such as "us" or "de".'
    )
    parser.add_argument(
        '--radius',
        type=str,
        default='smart',
        help='In miles for geographic requests, defaults to the member\'s preferred radius '
             'or 0.5 - maximum 100. May also be specified as "smart", '
             'a dynamic radius based on the number of active groups in the area.'
    )
    parser.add_argument(
        '--config',
        type=str,
        help='Filename for configuration file.',
        default='config.json'
    )
    args = parser.parse_args()
    
    query = {'location': args.city, 'country':args.country, 'radius': args.radius}

    with open(args.config) as conf_file:
        json_dict = json.load(conf_file)
       
    client = meetup.api.Client(json_dict['token'])
    topics = json_dict['topics']
    groups_properties = get_groups_properties(client, topics, query)

    df = make_dataframe(groups_properties)
    df.to_csv(args.output, encoding='utf-8')
